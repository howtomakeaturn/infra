# Infra project

- `helpers.php` file
- `AppCore.php` file
- layout + index + navbar + footer
- utils:
  - loading spinner
  - prevent double click
  - confirm popup

- boostrap@5.3.2
- jquery@.3.1
- fontawesome@6.5.1
- open-color@1.9.1
- toastr@2.1.3
- autosize@3.0.20

- laravel ui (user auth, must verify email.)
  - it's impossible to mangage fake accounts & spam posts without email verification.
  - so, please setup email credentials before allowing users to sign up.

- admin infra

- react@18
- react infra & settings

- prettier + eslint for react
- ref: https://www.aleksandrhovhannisyan.com/blog/format-code-on-save-vs-code-eslint/

- composer script: lint with pint
- for IDE: https://devinthewild.com/article/laravel-pint-formatting-vscode-phpstorm
