import ReactDOM from "react-dom/client";
import HelloWorld from "./components/HelloWorld";

ReactDOM.createRoot(document.getElementById("react-app")).render(
  <HelloWorld />,
);
