<div class="bg-dark text-white" id="layout-footer">
    <div class="container mt-5">
        <div class="mt-4 pt-4 pb-4">
            <div class="row">
                <div class="col">
                    <div style="font-size: 0.875rem;"><a href="/" class="text-white">INFRA PROJECT © {{ date("Y") }}</a></div>
                    <div class="mt-2" style="font-size: 0.875rem;">INFRA PROJECT</div>
                </div>
            </div>
        </div>
    </div>
</div>
